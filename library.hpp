///////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int search (Vector<generic> &, generic);
/// @brief Searches for the given item in the vector given. If found, it
/// the index of that item. Otherwise, it throws an ITEM_NOT_FOUND
/// exception.
/// @pre Vector exists
/// @post Function returns index of item, or throws exception if not found.
/// @param Vector to be search, item to be found 
/// @return unsigned int of inded of item.
///////////////////////////////////////////////////////////////////////////////
template <class generic>
unsigned int search (Vector <generic> & v, generic item)
{
  bool found = false; //did we find it?
  int index; //if so, where is it?

  for(int i = 0; i < v.size(); i++)
  {
    //Note that this will give you the location of the last item that matches
    //if there are duplicates
    if(v[i] == item)
    {
      found = true;
      index = i;
    }
  }
  if(found)
  {
    return index;
  }
  else//not found
  {
    throw Exception
    (
      ITEM_NOT_FOUND,
      "The requested item was not found in the vector."
    );
  }
}

///////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int binary_search (Vector<generic> &, generic);
/// @brief Searches for the given item in the vector given. If found, it
/// the index of that item. Otherwise, it throws an ITEM_NOT_FOUND
/// exception. This algorithm is more efficient than the regular search.
/// @pre Vector exists
/// @post Function returns index of item, or throws exception if not found.
/// @param Vector to be search, item to be found 
/// @return unsigned int of inded of item.
///////////////////////////////////////////////////////////////////////////////
template <class generic>
unsigned int binary_search (Vector<generic> & v, generic item)
{
  int min = 0;
  int max = (v.size() - 1);
  
  //You know that game where you guess what number the other person is thinking?
  //The most effient algorithm is to guess the number in the middle of 
  //the search range and work from there. That's what this is.
  int mid = (min + max) / 2;
  
  //While we haven't found it and we haven't crossed min and max 
  //(indicating that the item does not exist in this array)
  while(!(v[mid] == item) && (min < max))
  {    
    if( item > v[mid] )
    {
      min = mid + 1;
    }
    else
    {
      max = mid - 1;
    }
    mid = (min + max) / 2;//Set up for next go around.
  }
  
  //item found
  if(v[mid] == item)
  {
    return mid; //index
  }
  else //not found
  {
    throw Exception
    (
      ITEM_NOT_FOUND,
      "The requested item was not found in the vector."
    );
  }
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void bubble_sort (Vector<generic> &);
/// @brief This uses a bubble sort to sort the items in the vector from least 
/// to greatest.
/// @pre Vector exists.
/// @post Vector is sorted from least to greatest.
/// @param Vector to be sorted.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void bubble_sort (Vector<generic> & v)
{
  //If there's enough to sort
  if(v.size() < 2)
  {
    throw Exception
    (
      CANNOT_ORDER,
      "The vector contains less than two items."
    );
  }
  
  //Did we make a swap this pass? If so, do it again
  bool made_swap = true;
  while(made_swap)
  {
    made_swap = false; //Start with not making a swap
    
    //Go through every item in the array
    for(int i = 0; i < (v.size() - 1); i++)
    {
      if( v[i] > (v[i+1]) )//If not in right order (least to greatest)
      {
        //Swap them
        swap_vector(v, i, i+1);
     
        //Note that we swapped
        made_swap = true;
      }//if
    }//for
  }//while
}//bubble sort


///////////////////////////////////////////////////////////////////////////////
/// @fn void selection_sort (Vector<generic> &);
/// @brief This uses a selection sort to sort the items in the vector from least 
/// to greatest.
/// @pre Vector exists.
/// @post Vector is sorted from least to greatest.
/// @param Vector to be sorted.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void selection_sort (Vector<generic> & v)
{
  //If there's enough to sort
  if(v.size() < 2)
  {
    throw Exception
    (
      CANNOT_ORDER,
      "The vector contains less than two items."
    );
  }
  
  generic min; //lowest number
  int min_pos; //index of that number
  
  //For every item in the array
  for(int i = 0; i < (v.size() - 1); i++)
  {
    //Assume whatever we start is actually in the right place
    min = v[i];
    min_pos = i;
    
    //check every item in the unsorted side of the list
    for(int j = i+1; j < ( v.size() ); j++)
    {
      //We find something smaller than what we had before
      if(v[j] < min)
      {
        //Set it as new min
        min = v[j];
        min_pos = j;
      }      
    }
    //Now that we found the item that's *supposed* to be there...
    swap_vector(v, i, min_pos); //Put it there.
  }
  //Having now gone through every item, we're now done.
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void insertion_sort (Vector<generic> &);
/// @brief This uses a insertion sort to sort the items in the vector from least 
/// to greatest.
/// @pre Vector exists.
/// @post Vector is sorted from least to greatest.
/// @param Vector to be sorted.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void insertion_sort (Vector<generic> & v)
{
  //If there's enough to sort
  if(v.size() < 2)
  {
    throw Exception
    (
      CANNOT_ORDER,
      "The vector contains less than two items."
    );
  }
  
  
  //for each item excluding the first
  for(int i = 1; i < (v.size()); i++)
  {
    //Take the value at i
    int value = v[i];
    
    //Work backwards from i. 
    //If we haven't hit the back end and 
    //we need to move the value back from j
    for(int j = i - 1; j >= 0 && value < v[j]; j--)
    {
      //Swap the value back one.
      swap_vector(v, j, j+1);
    }
  }
  
}

/*
 * Not required functions implemented for my own amusement.
 */

///////////////////////////////////////////////////////////////////////////////
/// @fn void bogosort (Vector<generic> &);
/// @brief "Sorts" the vector by shuffling it and then checking to see if it's 
/// sorted. Worst case runtime is indefinite!
/// @pre Vector exists.
/// @post Vector is sorted from least to greatest.
/// @param Vector to be sorted.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void bogosort (Vector<generic> & v)
{
  //If there's enough to sort
  if(v.size() < 2)
  {
    throw Exception
    (
      CANNOT_ORDER,
      "The vector contains less than two items."
    );
  }
  
  while(!ordered_vector(v))
  {
    shuffle_vector(v);
  }  
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void quantum_bogosort (Vector<generic> &);
/// @brief An improvement on the bogosort, this function shuffles the vector,
/// then, if it is not sorted, it destroys the universe. Thus, from the
/// perspective of a programer who is in a parallel universe that was not 
/// destroyed, it works every time in O(2n).    
/// @pre Vector exists.
/// @post Vector is sorted from least to greatest.
/// @param Vector to be sorted.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void quantum_bogosort (Vector<generic> & v)
{
  //If there's enough to sort
  if(v.size() < 2)
  {
    throw Exception
    (
      CANNOT_ORDER,
      "The vector contains less than two items."
    );
  }
  
  shuffle_vector(v);
  if(!ordered_vector(v))
  {
    destroyTheUniverse();
  }
  
  /* Thus, from the perspective of those parallel universes 
   * which are not destroyed, this bogosort works every time, 
   * the first time, with a runtime of O(2n).
   */
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void destroyTheUniverse();
/// @brief This destroys the universe.
/// @pre Universe exists.
/// @post Universe does not exist.
/// @param None.
/// @return Nothing. (void)
///////////////////////////////////////////////////////////////////////////////
void destroyTheUniverse()
{
  //Implementation of this function is left as an exercise for the reader.
}


/*
 * Not required functions implemented to make testing easier.
 */

///////////////////////////////////////////////////////////////////////////////
/// @fn void print_vector (Vector<generic> &);
/// @brief Outputs the content of the vector for visual examiniation.
/// @pre Vector exists.
/// @post Content of vector is output to console.
/// @param Vector to be printed.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void print_vector (Vector<generic> & v)
{
  std::cout << std::endl;
  //Each item in the array
  for(int i = 0; i < v.size(); i++)
  {
    std::cout << v[i] << std::endl;
  }
  std::cout << std::endl;
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void fill_vector (Vector<generic> &);
/// @brief Clears vector and fills it from zero to length minus one.
/// @pre Vector exists.
/// @post Vector contains integers from 0 to length - 1
/// @param Vector to be filled, length to fill it to.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void fill_vector(Vector<generic> & v, int length)
{
  if(length < 0)
  {
    throw Exception
    (
      CONTAINER_EMPTY,
      "You have tried to create a vector of negative length."
    );
  }
  else
  {
    v.clear();
    for(int i = 0; i < length; i++)
    {
      v.push_back(i);
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void shuffle_vector (Vector<generic> &);
/// @brief Implements a Knuth shuffle on the vector.
/// @pre Vector exists.
/// @post Vector's contents are randomly shuffled.
/// @param Vector to be shuffled.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void shuffle_vector(Vector<generic> & v)
{
  //For each item in the array
  for(int i = 0; i < v.size(); i++)
  {
    //Swap the next element with a random element ahead (Knuth shuffle)
    swap_vector(v, i, ( rand() % (v.size() - i) ) + i );
  }
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void swap_vector (Vector<generic> &);
/// @brief Swaps the contents at two index values in a vector
/// @pre Vector exists, two legal index positions are given.
/// @post Contents at the listed index positions are swapped.
/// @param Vector to have values swapped, first and second indexes to swap.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////
template <class generic>
void swap_vector(Vector<generic> & v, int first, int second)
{
  generic temp;
  temp = v[first];
  v[first] = v[second];
  v[second] = temp;
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void ordered_vector (Vector<generic> &);
/// @brief Checks to see if vector is, in fact, ordered from least to greatest.
/// @pre Vector exists.
/// @post N/A
/// @param Vector to be checked
/// @return True if vector is in least to greatest order, false if not.
///////////////////////////////////////////////////////////////////////////////
template <class generic>
bool ordered_vector(Vector<generic> v)
{
  if(v.size() < 2)
  {
    throw Exception
    (
      CANNOT_ORDER,
      "The vector contains less than two items."
    );
  }
  bool ordered = true;
  for(int i = 0; i < (v.size() - 1); i++)
  {
    if(v[i] > (v[i+1]) )
    {
      ordered = false;
    }
  }
  return ordered;
}
