///////////////////////////////////////////////////////////////////////////////
/// @file test_library.cpp
/// @author David Norton :: CS153 Section 1A
/// @brief This is the implementation file for the header of the
/// test_library class, which performs unit testing on the sorting library.
///////////////////////////////////////////////////////////////////////////////

#include "test_library.h"
#include "vector.h"
#include <iostream>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION (Test_library);

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_bogosort();
/// @brief This is a dummy test of the bogosort -- no sane person would
/// actually conduct a bogosort, but I thought I'd write it up for fun.
/// This test actually does nothing.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

void Test_library::test_bogosort()
{
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_quantum_bogosort();
/// @brief This is a dummy test of the quantum bogosort -- no non-genocidal
/// person would actually conduct a quantum bogosort, but I thought I'd 
/// write it up for fun. This test actually does nothing.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

void Test_library::test_quantum_bogosort()
{
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_search();
/// @brief This tests the correctness of the search function. It instantiates
/// a vector, fills it and makes sure that the search function can find 
/// everything in it. It also tests for attempting to find something not
/// currently in the vector, which should throw an ITEM_NOT_FOUND exception.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

void Test_library::test_search()
{
  Vector <int> a;
  
  fill_vector(a, 10);
  
  //Show that the search function can find everything in the vector
  for(int i = 0; i < 10; i++)
  {
    CPPUNIT_ASSERT(a[search(a, i)] == i );
  }

  try
  {
    search(a, 10); //Ten does not exist
    CPPUNIT_FAIL("Should have thrown ITEM_NOT_FOUND exception.");
  }
  catch(Exception & e)
  {
    CPPUNIT_ASSERT(ITEM_NOT_FOUND == e.error_code());
  }
  
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_binary_search();
/// @brief This tests the correctness of the binary_search function. It 
/// instantiates a vector, fills it and makes sure that the search function 
/// can find everything in it. It also tests for attempting to find 
/// something not currently in the vector, which should throw an 
/// ITEM_NOT_FOUND exception.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

void Test_library::test_binary_search()
{
  Vector <int> a;
  
  fill_vector(a, 10);
  
  //Show that the search function can find everything in the vector
  for(int i = 0; i < 10; i++)
  {
    CPPUNIT_ASSERT(a[binary_search(a, i)] == i );
  }

  try
  {
    binary_search(a, 10); //Ten does not exist
    CPPUNIT_FAIL("Should have thrown ITEM_NOT_FOUND exception.");
  }
  catch(Exception & e)
  {
    CPPUNIT_ASSERT(ITEM_NOT_FOUND == e.error_code());
  }
  
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_bubble_sort();
/// @brief This tests the correctness of the bubble sort function. It 
/// instantiates a vector, fills it, shuffles it, and then has the bubble sort
/// create order out of the chaos. It then tests to make sure the vector was,
/// in fact, sorted properly.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

void Test_library::test_bubble_sort()
{

  Vector <int> a;
  a.push_back(9);
  
  try//sort unsortable vector
  {
    bubble_sort(a);
    CPPUNIT_ASSERT(0);
  }
  catch(Exception & e)
  {
    CPPUNIT_ASSERT( CANNOT_ORDER == e.error_code() );
  }
  
  fill_vector(a, 50);//Fill the vector with 50 digits, 0-49
  shuffle_vector(a); //Shuffle it
  //print_vector(a); //Allow visual inspection
  //CPPUNIT_ASSERT(!ordered_vector(a));//Test that it is not ordered
  
  bubble_sort(a);//Sort it from least to greatest
  //print_vector(a); //Allow visual inspection
  CPPUNIT_ASSERT(ordered_vector(a));//Test that it is ordered
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_selection_sort();
/// @brief This tests the correctness of the selection sort function. It 
/// instantiates a vector, fills it, shuffles it, and then the selection sort
/// creates order out of the chaos. It then tests to make sure the vector was,
/// in fact, sorted properly.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

void Test_library::test_selection_sort()
{
  Vector <int> a;
  a.push_back(9);
  
  try//sort unsortable vector
  {
    selection_sort(a);
    CPPUNIT_ASSERT(0);
  }
  catch(Exception & e)
  {
    CPPUNIT_ASSERT( CANNOT_ORDER == e.error_code() );
  }
  
  fill_vector(a, 50);//Fill the vector with 50 digits, 0-49
  shuffle_vector(a); //Shuffle it
  ///print_vector(a); //Allow visual inspection
  //CPPUNIT_ASSERT(!ordered_vector(a));//Test that it is not ordered
  
  
  selection_sort(a);//Sort it from least to greatest
  //print_vector(a); //Allow visual inspection
  CPPUNIT_ASSERT(ordered_vector(a));//Test that it is ordered

}

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_insertion_sort();
/// @brief This tests the correctness of the insertion sort function. It 
/// instantiates a vector, fills it, shuffles it, and then the insertion sort
/// creates order out of the chaos. It then tests to make sure the vector was,
/// in fact, sorted properly.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

void Test_library::test_insertion_sort()
{
  Vector <int> a;
  a.push_back(9);
  
  try//sort unsortable vector
  {
    insertion_sort(a);
    CPPUNIT_ASSERT(0);
  }
  catch(Exception & e)
  {
    CPPUNIT_ASSERT( CANNOT_ORDER == e.error_code() );
  }
  
  fill_vector(a, 50);//Fill the vector with 50 digits, 0-49
  shuffle_vector(a); //Shuffle it
  //print_vector(a); //Allow visual inspection
  //CPPUNIT_ASSERT(!ordered_vector(a));//Test that it is not ordered
  
  
  insertion_sort(a);//Sort it from least to greatest
  //print_vector(a); //Allow visual inspection
  CPPUNIT_ASSERT(ordered_vector(a));//Test that it is ordered
}


///////////////////////////////////////////////////////////////////////////////
/// @fn void test_print();
/// @brief This tests the correctness of the print function, by, well, printing
/// a vector so I can look at it myself. This is useful for debugging purposes.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

void Test_library::test_print ()
{
  //create vector
  Vector<int> a;

  //test print with nothing in it.
  print_vector(a);

  //test print with one item in it.
  a.push_back(2);
  print_vector(a);

  //test print with more than one item in it...
  a.push_back(1);
  a.push_back(0);

  print_vector(a);

}

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_fill();
/// @brief This tests the correctness of the fill function by testing the
/// vector that has been filled -- it should be the proper size and be ordered.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

void Test_library::test_fill ()
{
  Vector<int> a;

  try
  {
    fill_vector(a, -1);
    CPPUNIT_ASSERT(0);
  }
  catch(Exception & e)
  {
    CPPUNIT_ASSERT( CONTAINER_EMPTY == e.error_code() );
  }

  fill_vector(a, 5);

  CPPUNIT_ASSERT(a.size()==5);
  CPPUNIT_ASSERT(ordered_vector(a));
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_shuffle();
/// @brief This tests correctness of the shuffle function, by, well, printing
/// a vector so I can look at it myself. It instantiates a vector, fills it,
/// shuffles it, and then prints the shuffled vector.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

void Test_library::test_shuffle ()
{
  Vector<int> a;
  fill_vector(a, 5);
  shuffle_vector(a);

  print_vector(a);
}

///////////////////////////////////////////////////////////////////////////////
/// @fn void test_ordered();
/// @brief This tests correctness of the ordered vector function.
/// It instantiates a vector, fills it, and shows that the vector is already
/// ordered from least to greatest. It then performs a swap so that it is not
/// in order, and finally demonstrates that the ordered_vector properly reports
/// that the vector is not ordered least to greatest.
/// @pre Library class should already be declared
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
///////////////////////////////////////////////////////////////////////////////

void Test_library::test_ordered()
{
  Vector<int> a;
  a.push_back(9);
  //Can't order vector with less than two items
  try
  {
    ordered_vector(a);
    CPPUNIT_ASSERT(0);
  }
  catch(Exception & e)
  {
    CPPUNIT_ASSERT( CANNOT_ORDER == e.error_code() );
  }
  
  fill_vector(a, 10); //Fill with ten elements 0-9
  CPPUNIT_ASSERT(ordered_vector(a));
  swap_vector(a, 3, 8); //Swap something so it's not ordered
  CPPUNIT_ASSERT(!ordered_vector(a));
}
